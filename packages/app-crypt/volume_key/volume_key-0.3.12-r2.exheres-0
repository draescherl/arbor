# Copyright 2017-2022 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pagure [ suffix=tar.xz ]
require python [ blacklist=2 ]
require autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.16 ] ]

SUMMARY="Tool and library for manipulating and storing storage volume encryption keys"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8"
MYOPTIONS="
    ( libc: musl )
    ( linguas: as bg bn bn_IN ca cs de de_CH en_GB es eu fr gu hi hu id it ja kn ko ml mr nl or pa
               pl pt pt_BR ru sk sv ta te tr uk zh_CN zh_TW )
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.18.2]
        virtual/pkg-config[>=0.9.0]
    build+run:
        app-crypt/gnupg[>=2.0]
        app-crypt/gpgme[>=0.4.2]
        dev-libs/glib:2
        dev-libs/nss
        sys-apps/util-linux [[ note = [ libblkid ] ]]
        sys-fs/cryptsetup
        !libc:musl? ( dev-libs/libxcrypt:= )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-Add-3.7-to-the-list-of-python-versions.patch
    "${FILES}"/26c09768662d8958debe8c9410dae9fda02292c3.patch
    "${FILES}"/bf6618ec0b09b4e51fc97fa021e687fbd87599ba.patch
)

# Tests hang after completing, looks like gpg-agent stays around
RESTRICT="test"

configure_one_multibuild() {
    local params=(
        PYTHON3="/usr/$(exhost --target)/bin/python$(python_get_abi)"
        PYTHON3_CONFIG="/usr/$(exhost --target)/bin/python$(python_get_abi)-config"
        --enable-nls
        --with-gpgme-prefix="/usr/$(exhost --target)"
        --with-python3
        --without-python
    )

    econf "${params[@]}"
}

test_one_multibuild() {
    esandbox allow_net "unix:${TEMP}/.gnupg/S.gpg-agent*"
    esandbox allow_net --connect "unix:${TEMP}/.gnupg/S.gpg-agent*"

    default

    esandbox disallow_net --connect "unix:${TEMP}/.gnupg/S.gpg-agent*"
    esandbox disallow_net "unix:${TEMP}/.gnupg/S.gpg-agent*"
}

