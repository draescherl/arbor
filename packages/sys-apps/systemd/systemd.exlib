# Copyright 2010-2016 Wulf C. Krueger <philantrop@exherbo.org>
# Copyright 2017-2018 Jonathon Kowalski <worz@tuta.io>
# Distributed under the terms of the GNU General Public License v2

GITHUB_PROJECT=${PN}
GITHUB_BRANCH=main

require github [ project=${GITHUB_PROJECT} tag=v${PV} ] \
        meson [ cross_prefix=true ] \
        alternatives \
        kernel \
        test-dbus-daemon \
        udev-rules \
        pam \
        option-renames [ renames=[ 'journal-push libcurl' ] ] \
        providers \
        toolchain-funcs

require python [ blacklist="2 3.8" multibuild=false ]

export_exlib_phases pkg_pretend pkg_setup src_configure src_prepare src_test src_install pkg_postinst

SUMMARY="${PN} System and Service Manager"
DESCRIPTION="
systemd is a suite of basic building blocks for a Linux system. It provides a system
and service manager that runs as PID 1 and starts the rest of the system. systemd
provides aggressive parallelization capabilities, uses socket and D-Bus activation
for starting services, offers on-demand starting of daemons, keeps track of processes
using Linux control groups, supports snapshotting and restoring of the system state,
maintains mount and automount points and implements an elaborate transactional
dependency-based service control logic. Other parts include a logging daemon, utilities
to control basic system configuration like the hostname, date, locale, maintain a
list of logged-in users and running containers and virtual machines, system accounts,
runtime directories and settings, and daemons to manage simple network configuration,
network time synchronization, log forwarding, and name resolution.
SysVinit compatibility is deactivated in our package because we don't want it nor
do we support it.
"
HOMEPAGE="https://systemd.io/"

LICENCES="
    LGPL-2.1        [[ note = [ Everything but ] ]]
    GPL-2           [[ note = [ src/udev, ] ]]
    public-domain   [[ note = [ src/shared/MurmurHash2.c, src/shared/siphash24.c, src/journal/lookup3.c ] ]]
"
SLOT="0"
LANGUAGES=(
    be be@latin bg ca cs da de el es et eu fi fr gl he hi hr hu id it ja kab ka ko lt nl pa pl
    pt_BR pt ro ru si sk sl sr sv tr uk zh_CN zh_TW
)
MYOPTIONS="
    acl
    apparmor
    bash-completion
    cryptsetup [[ description = [ Enable systemd's minimal cryptsetup unit generator ] ]]
    efi [[
        description = [ EFI information in various tools, sd-boot, and mounting of efivars during boot ]
        note = [ Needs (U)EFI compatible hardware and a fairly recent kernel with proper configuration ]
    ]]
    gcrypt [[ description = [ Enable cryptographically secured journal files ] ]]
    gnutls [[
        description = [ Enable certificate support for journal-remote, journal-gatewayd & friends ]
        requires = [ gcrypt ]
    ]]
    homed [[
        description = [ Portable home directories support ]
        requires = [ openssl ]
    ]]
    idn [[ description = [ Support Internationalised Domain Names in systemd-resolved ] ]]
    importd [[
        description = [ Enable systemd's container download service systemd-importd ]
        requires = [ libcurl gcrypt ]
    ]]
    journal-gateway [[
        description = [ Enable journal gateway daemon to access the journal via HTTP and JSON ]
        requires = [ libcurl ]
    ]]
    libcurl [[ description = [ Support pushing journal data to a remote system ] ]]
    lz4 [[ description = [ Use LZ4 compression for longer journal fields ] ]]
    nat [[ description = [ Enable minimal firewall support for NAT ] ]]
    openssl [[ description = [ Enable openssl integration ] ]]
    pkcs11 [[ description = [ Native suppport for unlocking encrypted volumes utilizing PKCS#11 smartcards ] ]]
    polkit [[ description = [ Use PolicyKit for privileged operations ] ]]
    pstore [[
        description = [ Persistent storage filesystem archiving tool ]
        requires = [ acl lz4 ]
    ]]
    pwquality [[
        description = [ Password quality checking using libpwquality for systemd-homed ]
        requires = [ homed ]
    ]]
    qrencode [[ description = [ For transferring the journal verification key to a smartphone ] ]]
    regexp [[ description = [ regexp matching support using pcre2 in journalctl for the MESSAGE field ] ]]
    repart [[
        description = [ Build the systemd partitioning tool ]
        requires = [ openssl ]
    ]]
    seccomp [[ description = [ System call filtering support via seccomp ] ]]
    security-key [[
        description = [ FIDO2 support for systemd-homed / systemd-cryptenroll ]
        requires = [ openssl ]
    ]]
    selinux
    static [[ description = [ Build static versions of libsystemd and libudev and link systemctl, networkd and timesynd against them ] ]]
    tpm2 [[
        description = [ System measurements and encrypted volume unlocking using TPM2 ]
        requires = [ openssl ]
    ]]
    uki [[
        description = [ Tools for working with UEFI Unified Kernel Images ]
        requires = [ efi ]
    ]]
    xkbcommon [[ description = [ verify x11 keymap settings by compiling the configured keymap ] ]]
    zsh-completion
    zstd [[ description = [ Use Zstandard compression for longer journal fields ] ]]

    ( libc: musl )
    ( linguas: ${LANGUAGES[@]} )
    ( providers:
        gnutls [[ requires = [ gnutls ] ]]
        openssl [[ requires = [ openssl ] ]]
    ) [[
        number-selected = at-most-one
        *description = [ TLS provider for DNS-over-TLS ]
    ]]
"

DEPENDENCIES="
    build:
        app-text/docbook-xml-dtd:4.5 [[ note = [ for creating the man pages (used in {less-variables,standard-options}.xml) ] ]]
        dev-lang/perl:*
        dev-libs/libxslt [[ note = [ for creating the man pages ] ]]
        dev-python/Jinja2[python_abis:*(-)?]
        dev-util/gperf[>=3.0.4] [[ note = [ for keymap ] ]]
        sys-devel/gettext
        sys-kernel/linux-headers[>=3.10]
        virtual/pkg-config[>=0.20]
        efi? (
            dev-python/pyelftools[python_abis:*(-)?]
            sys-devel/binutils [[ note = [ efi binaries need to be linked with bfd/gold linker ] ]]
        )
    build+run:
        app-arch/xz
        dev-util/elfutils[>=0.158]
        sys-apps/kmod[>=15]
        sys-apps/pciutils
        sys-apps/skeleton-filesystem-layout
        sys-apps/util-linux[>=2.33]
        sys-kernel/linux-headers[>=3.10]
        sys-libs/libcap
        sys-libs/pam[>=1.1.2]
        !net-dns/nss-myhostname [[
            description = [ Included and enabled in systemd >= 197 ]
            resolution = uninstall-blocked-after
        ]]
        !sys-fs/udev [[
            description = [ udev is now part of systemd. ]
            resolution = uninstall-blocked-after
        ]]
        acl? ( sys-apps/acl )
        apparmor? ( security/apparmor[>=2.13] )
        cryptsetup? ( sys-fs/cryptsetup[>=2.4.0] )
        gcrypt? (
            dev-libs/libgcrypt[>=1.4.5]
            dev-libs/libgpg-error[>=1.12]
        )
        gnutls? ( dev-libs/gnutls[>=3.6.0] )
        idn? ( net-dns/libidn2:= )
        journal-gateway? ( net-libs/libmicrohttpd[>=0.9.33] )
        !libc:musl? ( dev-libs/libxcrypt:= )
        libcurl? ( net-misc/curl[>=7.32.0] )
        lz4? ( app-arch/lz4[>=1.7.5] )
        nat? ( net-firewall/iptables )
        pkcs11? ( dev-libs/p11-kit:1 )
        polkit? ( sys-auth/polkit:1 )
        providers:gnutls? ( dev-libs/gnutls[>=3.1.4] )
        providers:openssl? ( dev-libs/openssl:=[>=1.1.0] )
        pwquality? ( dev-libs/libpwquality[>=1.4.1] )
        qrencode? ( media-libs/qrencode:=[>=3] )
        regexp? ( dev-libs/pcre2 )
        seccomp? ( sys-libs/libseccomp[>=2.3.1] )
        tpm2? ( app-crypt/tpm2-tss[>=3.0.0] )
        uki? ( dev-python/pefile[python_abis:*(-)?] )
        xkbcommon? ( x11-libs/libxkbcommon[>=0.3.0] )
        security-key? ( dev-libs/libfido2 )
        selinux? ( security/libselinux )
        zstd? ( app-arch/zstd[>=1.4.0] )
    run:
        sys-apps/coreutils[selinux?][>=8.16]
        sys-apps/dbus[>=1.9.18] [[ note = [ Required to support the new DBus policy files location ] ]]
        sys-apps/kbd[>=1.15.2-r1]
        sys-apps/less[>=581] [[ note = [ Required for the auto-paging logic of the various tools ] ]]
        group/dialout
        group/lock [[ note = [ Required for var-lock service ] ]]
        group/render [[ note = [ Required for default udev-rules ] ]]
        group/sgx [[ note = [ Required for default udev-rules ] ]]
        group/systemd-bus-proxy [[ note = [ Required for systemd-bus-proxyd ] ]]
        group/systemd-coredump [[ note = [ Required for coredumpctl ] ]]
        group/systemd-journal [[ note = [ Required for journal access by non-root users when SplitMode=none or the journal is volatile ] ]]
        group/systemd-journal-remote [[ note = [ Required for systemd-journal-remote ] ]]
        group/systemd-network [[ note = [ Required for systemd-networkd ] ]]
        group/systemd-oom [[ note = [ Required for systemd-oom ] ]]
        group/systemd-resolve [[ note = [ Required for systemd-resolved ] ]]
        user/systemd-bus-proxy [[ note = [ Required for systemd-bus-proxyd ] ]]
        user/systemd-coredump [[ note = [ Required for coredumpctl ] ]]
        user/systemd-journal-remote [[ note = [ Required for systemd-journal-remote ] ]]
        user/systemd-network [[ note = [ Required for systemd-networkd ] ]]
        user/systemd-oom [[ note = [ Required for systemd-oom ] ]]
        user/systemd-resolve [[ note = [ Required for systemd-resolved ] ]]
        !sys-apps/debianutils[<4.5.1-r1] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
        !sys-apps/eudev [[
            description = [ ${PN} has the upstream udev daemon instead of Gentoo's fork eudev ]
            resolution = manual
        ]]
    suggestion:
        dev-python/lxml[python_abis:*(-)?] [[ description = [ Build the man page index ] ]]
        sys-apps/kexec-tools [[ description = [ Support for systemctl kexec - booting a kernel immediately, skipping the BIOS ] ]]
        sys-boot/dracut [[ description = [ Easily create an initramfs (if in doubt, don't take this) ] ]]
        uki? (
            dev-python/lz4[python_abis:*(-)?] [[ description = [ Parse lz4-compressed kernel when building UKI ] ]]
            dev-python/pyzstd[python_abis:*(-)?] [[ description = [ Parse zstd-compressed kernel when building UKI ] ]]
        )
"

systemd_pkg_pretend() {
    if ! kernel_version_at_least 4.15 ; then
        ewarn "You MUST install a kernel >= 4.15 to use systemd."
        ewarn "This check is based upon the kernel currently running, thus, if you already"
        ewarn "installed a suitable kernel and just need to boot it, you can disregard this."
    fi

    if [[ -f "${ROOT}"/etc/tmpfiles.d/legacy.conf ]] ; then
        ewarn "The configuration file /etc/tmpfiles.d/legacy.conf has been moved to"
        ewarn "/usr/$(exhost --target)/lib/tmpfiles.d/legacy.conf and can be safely removed after upgrade"
        ewarn "if you did not make any changes to it."
    fi

    option-renames_pkg_pretend
}

systemd_pkg_setup() {
    meson_pkg_setup

    exdirectory --allow /etc/binfmt.d
    exdirectory --allow /etc/modules-load.d
    exdirectory --allow /etc/sysctl.d
    exdirectory --allow /etc/systemd/system
    exdirectory --allow /etc/tmpfiles.d
    exdirectory --allow /etc/udev/rules.d
}

systemd_src_prepare() {
    meson_src_prepare

    if [[ -d ${FILES}/patches-${PV} ]]; then
        expatch -p1 "${FILES}"/patches-${PV}
    fi

    # Fails tests because our prefix makes --help outputs longer than 80 characters
    # Disable the test because it doesn't test actual functionality and is more of an upstream CI check
    edo ln -sf /bin/true tools/meson-check-help.sh

    # remove failing test, last checked: 244
    edo sed \
        -e '/test-user-util/,+3d' \
        -i src/test/meson.build

    # Respect selected python_abi
    edo sed \
        -e "s:/usr/bin/env python3:/usr/$(exhost --build)/bin/python$(python_get_abi):" \
        -i tools/*.py
    edo sed \
        -e "s:pymod.find_installation('python3':pymod.find_installation('python$(python_get_abi)':g" \
        -i meson.build
    edo sed \
        -e "s:/usr/bin/env python3:/usr/$(exhost --build)/bin/python$(python_get_abi):" \
        -i src/ukify/ukify.py \
        -i src/ukify/test/test_ukify.py

    # install to datadir instead of prefix
    edo sed \
        -e 's:mimepackagesdir = prefixdir:mimepackagesdir = datadir:g' \
        -e 's:share/mime/packages:mime/packages:g' \
        -i meson.build
}

MESON_SRC_CONFIGURE_PARAMS=(
    -Dadm-group=false
    -Danalyze=true
    -Daudit=disabled
    -Dbacklight=true
    -Dbinfmt=true
    -Dblkid=enabled
    -Dbpf-framework=disabled
    -Dbzip2=enabled
    -Dcertificate-root=/etc/ssl
    -Dcompat-mutable-uid-boundaries=true
    -Dcoredump=true
    -Dcreate-log-dirs=false
    -Ddbus=enabled
    -Ddebug-tty=/dev/tty9
    -Ddefault-dnssec=allow-downgrade
    -Ddefault-keymap=us
    -Ddefault-kill-user-processes=false
    -Ddefault-llmnr=yes
    -Ddefault-locale="C.UTF-8"
    -Ddefault-mdns=yes
    -Ddefault-mountfsd-trusted-directories=false
    -Ddefault-net-naming-scheme=latest
    -Ddefault-user-shell=/bin/bash
    -Ddocdir=/usr/share/doc/${PNVR}
    -Delfutils=enabled
    -Denvironment-d=true
    -Dfallback-hostname=localhost
    -Dfdisk=enabled
    -Dfexecve=false
    -Dfirst-boot-full-preset=false
    -Dfirstboot=true
    -Dfuzz-tests=false
    -Dglib=disabled
    -Dgshadow=true
    -Dhibernate=true
    -Dhostnamed=true
    -Dhtml=disabled
    -Dhwdb=true
    -Dima=false
    -Dinitrd=true
    -Dinstall-sysconfdir=true
    -Dinstall-tests=false
    -Dintegration-tests=false
    -Dkernel-install=true
    -Dkmod=enabled
    -Dldconfig=true
    -Dlibarchive=disabled
    -Dlibidn=disabled
    -Dlink-udev-shared=true
    -Dlocaled=true
    -Dlocalegen-path=""
    -Dlog-message-verification=disabled
    -Dlogind=true
    -Dmachined=true
    -Dman=enabled
    -Dmode=release
    -Dmountfsd=true
    -Dnetworkd=true
    -Dnobody-group=nobody
    -Dnobody-user=nobody
    -Dnspawn-locale="C.UTF-8"
    -Dnsresourced=true
    -Dnss-myhostname=true
    -Dnss-mymachines=enabled
    -Dnss-resolve=enabled
    -Dnss-systemd=true
    -Dntp-servers="0.exherbo.pool.ntp.org 1.exherbo.pool.ntp.org 2.exherbo.pool.ntp.org 3.exherbo.pool.ntp.org"
    -Doomd=true
    -Dpam=enabled
    -Dpamconfdir=/etc/pam.d
    -Dpamlibdir=/usr/$(exhost --target)/lib/security
    -Dpasswdqc=disabled
    -Dpkgconfigdatadir=/usr/$(exhost --target)/lib/pkgconfig
    -Dportabled=true
    -Dquotacheck=true
    -Drandomseed=true
    -Drc-local=""
    -Dresolve=true
    -Drfkill=true
    -Drpmmacrosdir=no
    -Dsbat-distro="exherbo"
    -Dservice-watchdog=3min
    -Dslow-tests=false
    -Dsmack=false
    -Dsplit-bin=false
    -Dsshconfdir=no
    -Dsshdconfdir=no
    -Dsshdprivsepdir=no
    -Dstandalone-binaries=false
    -Dstatus-unit-format-default=combined
    -Dstoragetm=false
    -Dsysext=true
    -Dsysupdate=disabled
    -Dsysusers=true
    -Dsysvinit-path=""
    -Dsysvrcnd-path=""
    -Dtests=true
    -Dtimedated=true
    -Dtimesyncd=true
    -Dtmpfiles=true
    -Dtpm=true
    -Dtranslations=true
    -Dtty-gid=5
    -Durlify=true
    -Dutmp=true
    -Dvconsole=true
    -Dvcs-tag=false
    -Dvmlinux-h=disabled
    -Dvmlinux-h-path=''
    -Dvmspawn=disabled
    -Dwheel-group=true
    -Dxdg-autostart=true
    -Dxenctrl=disabled
    -Dxinitrcdir=/etc/X11/xinit/xinitrc.d
    -Dxz=enabled
    -Dzlib=enabled
)

if ever at_least 257; then
    MESON_SRC_CONFIGURE_PARAMS+=(
        -Dipe=false
        -Dsysupdated=disabled
        -Dshellprofiledir=/etc/profile.d
        -Dtty-mode=0620
    )
else
    MESON_SRC_CONFIGURE_PARAMS+=(
        -Dnscd=true
    )
fi

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'bash-completion bashcompletiondir /usr/share/bash-completion/completions no'
    efi
    'homed userdb'
    idn
    pstore
    'static link-boot-shared false true'
    'static link-executor-shared false true'
    'static link-journalctl-shared false true'
    'static link-networkd-shared false true'
    'static link-portabled-shared false true'
    'static link-systemctl-shared false true'
    'static link-timesyncd-shared false true'
    'static static-libsystemd pic false'
    'static static-libudev pic false'
    'zsh-completion zshcompletiondir /usr/share/zsh/site-functions no'
)

MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    acl
    apparmor
    'cryptsetup libcryptsetup'
    'cryptsetup libcryptsetup-plugins'
    'efi bootloader'
    gcrypt
    gnutls
    homed
    'idn libidn2'
    importd
    'journal-gateway microhttpd'
    'journal-gateway remote'
    libcurl
    lz4
    'nat libiptc'
    openssl
    'pkcs11 p11kit'
    polkit
    pwquality
    qrencode
    'regexp pcre2'
    repart
    seccomp
    'security-key libfido2'
    selinux
    tpm2
    'uki ukify'
    xkbcommon
    zstd
)

systemd_src_configure() {
    if option efi; then
        providers_set 'ld bfd'
        providers_set 'compiler-tools binutils'
    fi

    local meson_args=()

    if option providers:gnutls; then
        meson_args+=(
            '-Dcryptolib=gcrypt'
            '-Ddns-over-tls=gnutls'
            '-Ddefault-dns-over-tls=opportunistic'
        )
    elif option providers:openssl; then
        meson_args+=(
            '-Dcryptolib=openssl'
            '-Ddns-over-tls=openssl'
            '-Ddefault-dns-over-tls=opportunistic'
        )
    else
        meson_args+=(
            '-Ddns-over-tls=false'
            '-Ddefault-dns-over-tls=no'
        )
    fi

    meson_src_configure "${meson_args[@]}"
}

systemd_src_test() {
    if exhost --is-native -q; then
        # Note to self & whoever else reads this: Don't even *think* about
        # RESTRICT=userpriv in order to enable more expensive udev tests.
        # Enabling those tests can (and most likely will) mess up your running system
        # completely and require you to reboot. You've been warned.
        if [[ -f /etc/machine-id ]]; then
            # src/test/test-systemd-tmpfiles.py expects ${HOME} to not end in a slash
            HOME=${HOME%/}

            # src/test/test-condition.c gets confused when running with ${USER} == root && euid != 0
            # can be removed when this paludis MR is merged:
            # https://gitlab.exherbo.org/paludis/paludis/merge_requests/4
            USER=paludisbuild

            # The tests currently fail if run under sydbox.
            esandbox disable
            esandbox disable_net
            test-dbus-daemon_run-tests meson_src_test
            esandbox enable
            esandbox enable_net
        else
            ewarn "The tests require a valid, initialised /etc/machine-id which you don't seem to"
            ewarn "have. Please run /usr/bin/systemd-machine-id-setup and re-install systemd if you"
            ewarn "want to run the tests."
        fi
    else
        echo "cross compiled host, skipping tests"
    fi
}

systemd_src_install() {
    local host=$(exhost --target) alternatives=(
        init ${PN} 1000
        /usr/share/man/man1/init.1 ${PN}.init.1
    )

    local a manpages=()
    manpages=( halt poweroff reboot shutdown )
    for a in ${manpages[@]} ; do
        alternatives+=(
            /usr/share/man/man8/${a}.8 ${PN}.${a}.8
        )
    done

    meson_src_install

    # alternatives
    local a
    for a in halt poweroff reboot runlevel shutdown telinit; do
        dosym systemctl /usr/${host}/bin/${a}
        alternatives+=(
            /usr/${host}/bin/${a} ${PN}.${a}
        )
    done
    dosym ../lib/systemd/systemd /usr/${host}/bin/init
    alternatives+=(
        /usr/${host}/bin/init ${PN}.init
    )

    keepdir /usr/${host}/lib/systemd/user-generators
    keepdir /usr/${host}/lib/udev/devices
    keepdir /usr/${host}/lib/systemd/system/graphical.target.wants
    keepdir /usr/${host}/lib/systemd/system-generators
    keepdir /usr/${host}/lib/systemd/system-shutdown
    keepdir /usr/${host}/lib/systemd/user-generators
    keepdir /usr/${host}/lib/environment.d
    keepdir /usr/${host}/lib/modules-load.d
    keepdir /usr/${host}/lib/sysctl.d
    keepdir /usr/${host}/lib/binfmt.d
    keepdir /usr/${host}/lib/tmpfiles.d
    keepdir /usr/${host}/lib/systemd/system-sleep
    keepdir /usr/share/user-tmpfiles.d

    alternatives_for "${alternatives[@]}"

    alternatives_for installkernel ${PN} 100 \
        /usr/$(exhost --target)/bin/installkernel kernel-install

    insinto /etc

    # Install a sample vconsole file
    hereins vconsole.conf <<EOF
# The console font to use.
# If you want to use your kernel's defaults, comment out everything here.
#FONT=
FONT="lat9w-16"
# The charset map file to use. Look in /usr/share/consoletrans for map files.
#FONT_MAP=""
#FONT_UNIMAP=""
# The keyboard layout to use.
KEYMAP="us"
#KEYMAP_TOGGLE=""
EOF

    # Install a default hostname file
    hereins hostname <<EOF
localhost
EOF

    # Install a default machine-info file
    hereins machine-info <<EOF
# A human-readable UTF-8 machine identifier string. This should contain a name like
# "Wulf's Notebook" which should be similar to the hostname (e. g. "wulfs-notebook")
# but may differ if you prefer because it's used for presentation only (e. g. in GDM/KDM).
PRETTY_HOSTNAME="My Computer"
# An icon identifying this machine according to the XDG Icon Naming Specification.
# http://standards.freedesktop.org/icon-naming-spec/icon-naming-spec-latest.html
# The default value "computer" is the most basic fallback, you could use e. g.
# "computer-laptop" or "computer-desktop".
ICON_NAME=computer
EOF

    # Install a default *system* locale file
    hereins locale.conf <<EOF
# Here you configure the *system* locale, i. e. the locale daemons and other non-
# interactive processes get. *Never* change anything here if you don't know *exactly*
# what you're doing. For your user's locale, use an /etc/env.d file instead.
# You must not use LC_ALL here.
LANG=
LC_CTYPE=en_US.UTF-8
LC_NUMERIC=C
LC_TIME=C
LC_COLLATE=C
LC_MONETARY=C
LC_MESSAGES=C
LC_PAPER=C
LC_NAME=C
LC_ADDRESS=C
LC_TELEPHONE=C
LC_MEASUREMENT=C
LC_IDENTIFICATION=C
EOF

    elog "Persistent logging to the systemd journal in /var/log/journal is disabled"
    elog "by default because it is felt that logging decisions are best left to the"
    elog "sysadmin."
    elog ""
    elog "For information on how to enable persistent logging, please consult the"
    elog "systemd-journald man page."

    # keepdir some stuff
    keepdir /etc/systemd/session
    keepdir /etc/systemd/system/getty.target.wants
    keepdir /etc/systemd/system/graphical.target.wants
    keepdir /etc/systemd/system/local-fs.target.wants
    keepdir /etc/systemd/system/multi-user.target.wants
    keepdir /etc/systemd/system/sysinit.target.wants
    keepdir /etc/systemd/user
    keepdir /var/lib/systemd

    # Make sure /etc/machine-id exists.
    [[ -f /etc/machine-id ]] || edo touch "${IMAGE}"/etc/machine-id

    # Keep the administrator's environment.d configuration dir.
    keepdir /etc/environment.d

    # Module names in /etc/modules-load.d/?*.conf get read and the modules loaded.
    keepdir /etc/modules-load.d

    # Files in /etc/sysctl.d/?*.conf get read and applied via sysctl. Can be used
    # in combination with sysctl.conf (sysctl.conf takes precedence over sysctl.d).
    keepdir /etc/sysctl.d

    # Files in /etc/binfmt.d/?*.conf contain a list of binfmt_misc kernel binary
    # format rules. Those are used to configure additional binary formats to register
    # during boot in the kernel.
    keepdir /etc/binfmt.d

    # Files in /etc/tmpfiles.d/?*.conf contain a list of files and/or directories.
    # Those are automatically (re)created, removed, truncated,... during boot or after a specified time
    # with specified owner, group and access mode.
    keepdir /etc/tmpfiles.d

    # Helper dirs for the kernel-install utility.
    keepdir /etc/kernel/install.d
    keepdir /usr/${host}/lib/kernel/install.d

    # udev link-config rules files
    keepdir /etc/systemd/network
    keepdir /usr/${host}/lib/systemd/network

    # Keep the credstore directories.
    keepdir /usr/${host}/lib/credstore
    # Already handled by tmpfiles.d/credstore.conf
    edo rmdir "${IMAGE}"/etc/credstore{,.encrypted}

    # Keep the administrator's udev rules dir.
    keepdir /etc/udev/rules.d

    # Keep the administrator's udev hardware database dir.
    keepdir /etc/udev/hwdb.d

    # Keep the administrator's override directives directories.
    keepdir /etc/systemd/{coredump,journald,logind,resolved,system,timesyncd,user}.conf.d

    # module loading configuration
    insinto /etc/modprobe.d
    doins "${FILES}"/blacklist.conf

    # config protection
    hereenvd 20udev <<EOT
CONFIG_PROTECT_MASK="${UDEVRULESDIR}"
EOT

    # Create compatibility symlinks
    dosym ../lib/systemd/systemd-udevd /usr/${host}/bin/udevd

    # Exclude /var/tmp/paludis/build from cleaning
    edo echo "x /var/tmp/paludis/build" >> "${IMAGE}"/usr/${host}/lib/tmpfiles.d/tmp.conf

    # Remove unselected languages
    for i in "${LANGUAGES[@]}"; do
        local objects=()
        objects=(
            "${IMAGE}"/usr/${host}/lib/systemd/catalog/systemd.${i}.catalog
            "${IMAGE}"/usr/share/locale/${i}
        )
        if ! option "linguas:${i}"; then
            for o in "${objects[@]}"; do
                [[ -e ${o} ]] && edo rm -r "${o}"
            done
            # Remove the mortal remains, empty shells of their former glory... ;-)
            edo find "${IMAGE}"/usr/share/locale -type d -empty -delete
        fi
    done
}

systemd_pkg_postinst() {
    default

    # We're running some systemd commands here which make some very fundamental changes to the
    # system. It's very difficult to figure out what exactly systemd/systemctl/etc. need to be able
    # to access or write in order to work correctly, and this changes from version to version. It is
    # simplest to run the postinst steps which use installed systemd commands without sandboxing.
    esandbox disable
    esandbox disable_net

    if exhost --is-native -q && [[ ${ROOT} == / ]]; then
        nonfatal edo /usr/${CHOST}/bin/systemd-machine-id-setup || ewarn "systemd-machine-id-setup failed"
        nonfatal edo mkdir -p /var/lib/dbus || ewarn "mkdir /var/lib/dbus failed"
        nonfatal edo ln -snf /etc/machine-id /var/lib/dbus/machine-id || ewarn "creating machine-id symlink failed"

        # systemd >= 215 expects /usr to be touched at install phase
        nonfatal edo touch /usr

        # Note(alip): The chroot test will not work under syd-3,
        # due to procfs hardening. To workaround this, we create
        # a script and make the check outside the sandbox.
        # Note(alip): This method should work for syd-{1,3} and
        # we do not support syd-0 any longer.
        script="${TEMP}/restart.bash"
        signal="${TEMP}/restart.done"

        cat >"${script}" <<EOF
#!/bin/bash -x
# if the root of init does not match our root, we are in a chroot and should not perform the
# restart of the udev process
if [[ -r /proc/1/root && /proc/1/root -ef /proc/self/root/ ]]; then
    if [[ -S /run/systemd/private ]]; then
        # We are running systemd, use systemctl
        systemctl --system daemon-reexec
        systemctl --system restart systemd-udevd.service
        systemctl --system restart systemd-journald.service
    else
        # No need to ewarn or something because udevd might not be running.
        pkill -TERM udevd
        sleep 1
        pkill -KILL udevd

        /usr/${CHOST}/bin/udevd --daemon || echo >&2 "udevd couldn't be restarted"
    fi
fi
touch "${signal}"
EOF
        edo chmod +x "${script}"

        # Two things to note:
        # 1. esandbox exec executes process in the background.
        # 2. esandbox exec executes process with CWD set to /.
        # TODO: Document these in Exheres for Smarties.
        esandbox exec "${script}"
        # Let's wait for a minute for the restart to complete.
        for _ in {1..60}; do
            test -e "${signal}" && break
            sleep 1
        done
    fi

    # Update the hwdb index
    nonfatal edo /usr/${CHOST}/bin/systemd-hwdb --root="${ROOT}" --usr update || ewarn "Updating hwdb failed (systemd-hwdb --usr update)"

    # Update the message catalogue index
    nonfatal edo /usr/${CHOST}/bin/journalctl --root="${ROOT}" --update-catalog || ewarn "Updating journal message catalogue failed (journalctl --update-catalog)"

    esandbox enable
    esandbox enable_net

    # remove the old directories so systemd can automatically create symlinks for them pointing
    # to the new location under /var/lib/private/systemd.
    if [[ ! -L /var/lib/systemd/journal-upload ]] ; then
        nonfatal edo rm -rf /var/lib/systemd/journal-upload
    fi
    # timesyncd changed the place where it stores its state
    if [[ -L /var/lib/systemd/timesync ]] ; then
        nonfatal edo rm /var/lib/systemd/timesync
        nonfatal edo rm -rf /var/lib/private/systemd/timesync
    fi
    # Earlier the location was /var/lib/systemd/clock, so let's also clean it up.
    # https://github.com/systemd/systemd/commit/53d133ea1bb4c4ed44c4b6aae42f1feb33d9cb78
    if [[ -f /var/lib/systemd/clock ]] ; then
        nonfatal edo rm -f /var/lib/systemd/clock
    fi

    if has_version 'sys-apps/systemd[>=242]'; then
        ewarn "Prior to systemd 242, some services were enabled by default."
        ewarn "You now need to enable those manually if you want to use them:"
        ewarn "systemd-networkd systemd-timesyncd systemd-resolved getty@tty1"
    fi

    local cruft=(
        "${ROOT}"/etc/init.d/sysinit.bash
        "${ROOT}"/etc/systemd/{systemd-journald.conf,systemd-logind.conf}
        "${ROOT}"/etc/udev/hwdb.bin
    )
    for file in "${cruft[@]}"; do
        if [[ -f ${file} || -L ${file} ]]; then
            nonfatal edo rm "${file}" || ewarn "removing ${file} failed"
        fi
    done

    local renamed=( "${ROOT}"/etc/locale "${ROOT}"/etc/vconsole )
    for file in "${renamed[@]}"; do
        if [[ -f ${file} ]] ; then
            nonfatal edo mv "${file}" "${file}".conf || ewarn "moving ${file} failed"
        fi
    done

    alternatives_pkg_postinst
}

