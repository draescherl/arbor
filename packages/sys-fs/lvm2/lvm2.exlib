# Copyright 2008 Kim Højgaard-Hansen <kimrhh@exherbo.org>
# Copyright 2009-2015 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require systemd-service udev-rules
require openrc-service [ openrc_confd_files=[ "${FILES}"/openrc/confd ] ]

export_exlib_phases src_configure src_compile src_install

SUMMARY="Userspace toolset providing logical volume management facilities"
DESCRIPTION="
LVM2 is a logical volume manager for the Linux kernel: it manages disk drives and
similar mass-storage devices, in particular large ones. The term \"volume\" refers
to a disk drive or part thereof.
This package also includes device-mapper.
"
HOMEPAGE="https://sourceware.org/${PN}/"
DOWNLOADS="
    mirror://sourceware/${PN}/LVM2.${PV}.tgz
    mirror://sourceware/${PN}/old/LVM2.${PV}.tgz
"
UPSTREAM_RELEASE_NOTES="https://sourceware.org/git/?p=${PN}.git;a=blob;f=WHATS_NEW;hb=v${PV//./_}"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    dmeventd [[ description = [ DM event daemon to monitor active mapped devices ] ]]
    fsadm [[ description = [ Utility to resize or check filesystem on a device ] ]]
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

# The tests are expensive, unreliable, they need write access to /dev and /etc
# and leave stale volumes lingering around if they fail
RESTRICT="test"

DEPENDENCIES="
    build:
        sys-devel/autoconf-archive [[ note = [ required for AC_PYTHON_MODULE ] ]]
        virtual/pkg-config
    build+run:
        dev-libs/libaio
        sys-apps/util-linux[>=2.24]
        sys-libs/readline:=
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd[>=234] )
"

WORK="${WORKBASE}"/LVM2.${PV}

# TODO: package https://github.com/dm-vdo/vdo for vdoformat
# --with-vdo=internal
# --with-vdo-format=/usr/$(exhost --target)/bin/vdoformat
# TODO: package https://github.com/jthornber/thin-provisioning-tools
# --with-cache=internal
# --with-thin=internal
lvm2_src_configure() {
    local myconf=(
        RUN_DIR=/run
        --enable-blkid_wiping
        --enable-blkzeroout
        --enable-cmdlib
        --enable-lvmimportvdo
        --enable-lvmpolld
        --enable-pkgconfig
        --enable-readline
        --enable-shared
        --enable-udev_rules
        --enable-udev_sync
        --disable-dbus-service
        --disable-editline
        --disable-lvmlockd-idm
        --disable-lvmlockd-dlm
        --disable-lvmlockd-dlmcontrol
        --disable-lvmlockd-sanlock
        --disable-nls
        --disable-notify-dbus
        --disable-selinux
        --disable-static_link
        --with-blkid
        --with-cache=none
        --with-cmirrord-pidfile=/run/cmirrord.pid
        --with-dmeventd-pidfile=/run/dmeventd.pid
        --with-default-locking-dir=/run/lock/lvm
        --with-default-run-dir=/run/lvm
        --with-integrity=internal
        --with-mirrors=internal
        --with-modulesdir=/usr/$(exhost --target)/lib/modules
        --with-thin=none
        --with-tmpfilesdir="${SYSTEMDTMPFILESDIR}"
        --with-snapshots=internal
        --with-udev
        --with-udevdir="${UDEVRULESDIR}"
        --with-vdo=none
        --with-writecache=internal
        $(option_enable dmeventd)
        $(option_enable fsadm)
        $(option_enable providers:systemd app-machineid)
        $(option_enable providers:systemd systemd-journal)
        $(option_with providers:systemd systemd)
        $(option_with providers:systemd systemd-run /usr/$(exhost --target)/bin/systemd-run)
    )

    if [[ $(exhost --target) == *-musl* ]]; then
        myconf+=(
            --with-symvers=no
        )
    fi

    if ! exhost --is-native -q ; then
        myconf+=(
            ac_cv_func_malloc_0_nonnull=yes
            ac_cv_func_realloc_0_nonnull=yes
        )
    fi

    econf "${myconf[@]}"
}

lvm2_src_compile() {
    # Confuses the build system, causing a build failure, see arbor#31
    edo unset TARGETS

    default
}

lvm2_src_install() {
    # Confuses the build system, causing a build failure, see arbor#31
    edo unset TARGETS

    default

    emake DESTDIR="${IMAGE}" install_systemd_generators
    emake DESTDIR="${IMAGE}" install_systemd_units
    emake DESTDIR="${IMAGE}" install_tmpfiles_configuration

    install_systemd_files
    install_openrc_files
}

