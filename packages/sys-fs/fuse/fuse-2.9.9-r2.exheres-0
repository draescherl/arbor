# Copyright 2009 Maxime Coste <frrrwww@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require github [ user=lib${PN} project=lib${PN} release=${PNV} suffix=tar.gz ]
require autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.16 ] ]

SUMMARY="Filesystems in Userspace"
DESCRIPTION="
FUSE (Filesystem in Userspace) is a simple interface for userspace programs to export a virtual
filesystem to the Linux kernel. FUSE also aims to provide a secure method for non privileged users
to create and mount their own filesystem implementations.
"

LICENCES="GPL-2 LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    run:
        sys-apps/util-linux[>=2.18]
        !sys-fs/fuse:0[<2.9.7-r1] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/6d55007027dfe7b75a74899f497f075046cc5404.patch
    "${FILES}"/5d38afc8a5b4a2a6e27aad7a1840046e99cd826d.patch
    "${FILES}"/5a43d0f724c56f8836f3f92411e0de1b5f82db32.patch
    "${FILES}"/${PN}-fuse_kernel.h-clean-includes.patch
    -p2 "${FILES}"/${PN}-2.8.6-gold.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    MOUNT_FUSE_PATH=/usr/$(exhost --target)/bin
    --disable-example
    --disable-static
)

src_install() {
    default

    edo rm -r "${IMAGE}"/etc
    edo rm -r "${IMAGE}"/dev
}

pkg_preinst() {
    # Remove old alternatives symlink
    if [[ -L "${ROOT}"/usr/share/man/man8/mount.fuse.8 ]]; then
        nonfatal edo rm "${ROOT}"/usr/share/man/man8/mount.fuse.8
    fi
}

