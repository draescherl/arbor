# Copyright 2008 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require alternatives

SUMMARY="Standalone IPC library used by gpg, gpgme and newpg"
HOMEPAGE="https://www.gnupg.org/related_software/${PN}"
DOWNLOADS="mirror://gnupg/${PN}/${PNV}.tar.bz2"

LICENCES="GPL-3 LGPL-2.1"
SLOT="9"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-libs/libgpg-error[>=1.17]
    run:
        !dev-libs/libassuan:0[<2.5.7-r1] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --with-libgpg-error-prefix=/usr/$(exhost --target)
)

DEFAULT_SRC_COMPILE_PARAMS=(
    CC_FOR_BUILD=$(exhost --build)-cc
)

src_test() {
    esandbox allow_net "unix:${WORK}/tests/a.sock"

    default

    esandbox disallow_net "unix:${WORK}/tests/a.sock"
}

src_install() {
    local arch_dependent_alternatives=() other_alternatives=()
    local host=$(exhost --target)

    default

    arch_dependent_alternatives+=(
        /usr/${host}/bin/${PN}-config       ${PN}-config-${SLOT}
        /usr/${host}/include/assuan.h       assuan-${SLOT}.h
        /usr/${host}/lib/${PN}.la           ${PN}-${SLOT}.la
        /usr/${host}/lib/${PN}.so           ${PN}-${SLOT}.so
        /usr/${host}/lib/pkgconfig/${PN}.pc ${PN}-${SLOT}.pc
    )

    other_alternatives+=(
        /usr/share/aclocal/${PN}.m4 ${PN}-${SLOT}.m4
        /usr/share/info/assuan.info assuan-${SLOT}.info
    )

    alternatives_for _${host}_${PN} ${SLOT} ${SLOT} "${arch_dependent_alternatives[@]}"
    alternatives_for _${PN} ${SLOT} ${SLOT} "${other_alternatives[@]}"
}

