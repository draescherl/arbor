# Copyright 2009-2011 Wulf C. Krueger
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'pkcs11-helper-1.06.ebuild' from Gentoo, which is:
#       Copyright 1999-2008 Gentoo Foundation

require github [ user=OpenSC release=${PNV} suffix=tar.bz2 ]

SUMMARY="PKCS#11 helper library"
DESCRIPTION="
pkcs11-helper is a library that simplifies the interaction with PKCS#11
providers for end-user applications using a simple API and optional OpenSSL
engine.
"

UPSTREAM_CHANGELOG="https://github.com/OpenSC/pkcs11-helper/blob/master/ChangeLog"

LICENCES="|| ( BSD-3 GPL-2 )"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    doc gnutls mbedtls nss
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
        doc? ( app-doc/doxygen[>=1.4.7] )
    build+run:
        gnutls? ( dev-libs/gnutls[>=2.5.4] )
        mbedtls? ( dev-libs/mbedtls )
        nss? ( dev-libs/nss[>=3.12-r1] )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-crypto-engine-openssl
    --disable-static
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    doc
    "gnutls crypto-engine-gnutls"
    "mbedtls crypto-engine-mbedtls"
    "nss crypto-engine-nss"
)

src_install() {
    default

    edo rm "${IMAGE}"/usr/share/doc/${PNVR}/COPYING*
}

