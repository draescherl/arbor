# Copyright 2018 Tom Briden <tom@decompile.me.uk>
# Distributed under the terms of the GNU General Public License v2

require github [ user=JuliaStrings tag=v${PV} ] cmake

SUMMARY="Clean C library for processing UTF-8 Unicode data"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS=""

DEPENDENCIES=""

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_SHARED_LIBS:BOOL=TRUE
    -DLIB_FUZZING_ENGINE:BOOL=FALSE
    -DUTF8PROC_INSTALL:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_TESTS=(
    '-DUTF8PROC_ENABLE_TESTING:BOOL=TRUE -DUTF8PROC_ENABLE_TESTING:BOOL=FALSE'
)

UNICODE_VERSION="15.0.0"

src_fetch_extra() {
    # Download test data so cmake doesn't have to download it later during
    # (sandboxed) src_test
    if expecting_tests ; then
        local file="NormalizationTest-${UNICODE_VERSION}.txt"
        if [[ ! -e "${FETCHEDDIR}"/${file} ]] ; then
            edo wget -O "${FETCHEDDIR}"/${file} \
                https://www.unicode.org/Public/${UNICODE_VERSION}/ucd/NormalizationTest.txt
        fi

        local file="GraphemeBreakTest-${UNICODE_VERSION}.txt"
        if [[ ! -e "${FETCHEDDIR}"/${file} ]] ; then
            edo wget -O "${FETCHEDDIR}"/${file} \
                https://www.unicode.org/Public/${UNICODE_VERSION}/ucd/auxiliary/GraphemeBreakTest.txt
        fi
    fi
}

src_unpack() {
    cmake_src_unpack

    if expecting_tests ; then
        edo mkdir "${WORK}"/data
        edo cp "${FETCHEDDIR}"/{Normalization,GraphemeBreak}Test-${UNICODE_VERSION}.txt \
            "${WORK}"/data/
        edo mv "${WORK}"/data/NormalizationTest{-${UNICODE_VERSION},}.txt
        edo mv "${WORK}"/data/GraphemeBreakTest{-${UNICODE_VERSION},}.txt
    fi
}

src_prepare() {
    cmake_src_prepare

    # Don't download test data already downloaded during src_fetch_extra
    edo sed -e "/file(.*DOWNLOAD /d" -i CMakeLists.txt
}

