# Copyright 2009 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2013-2023 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require sourceforge [ project=${PN,,} suffix=tar.xz ]

SUMMARY="GraphicsMagick Image Processing System"
DESCRIPTION="
GraphicsMagick is the swiss army knife of image processing. Comprised of 337K
lines of C and C++ code, it provides a robust and efficient collection of tools
and libraries which support reading, writing, and manipulating an image in over
88 major formats including important formats like DPX, GIF, JPEG, JPEG-2000,
PNG, PDF, PNM, and TIFF.

GraphicsMagick is originally derived from ImageMagick 5.5.2 but has been
completely independent of the ImageMagick project since then. Since the fork
from ImageMagick in 2002, many improvements have been made (see news) by many
authors using an open development model but without breaking the API or
utilities operation. (More on the homepage)
"
HOMEPAGE="http://www.graphicsmagick.org"

UPSTREAM_CHANGELOG="${HOMEPAGE}/Changelog.html"
UPSTREAM_RELEASE_NOTES="${HOMEPAGE}/NEWS.html"
UPSTREAM_DOCUMENTATION="${HOMEPAGE}/FAQ.html [[ lang = en description = [ FAQ ] ]]"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    X
    doc
    fpx [[ description = [ Support for the FlashPix image format ] ]]
    heif [[ description = [ Support for the H(igh) E(fficiency) I(mage) F(ile) F(ormat) ] ]]
    imagemagick [[ description = [ Install ImageMagick utility shortcuts ] ]]
    jpeg2000
    jpegxl [[ description = [ Support for the JPEG XL image file format ] ]]
    lcms
    openmp [[ description = [ Support for Open Multi-Processing ] ]]
    tiff
    truetype
    webp
    wmf [[ description = [ Support for the Windows Metafile image format ] ]]
    zstd
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

# Tests fail with different compile time options not enabled
RESTRICT=test

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        app-arch/bzip2
        app-arch/xz
        dev-libs/libxml2:2.0[>=2.7.8]
        media-libs/libpng:=
        sys-libs/zlib
        X?  (
            x11-libs/libSM
            x11-libs/libICE
            x11-libs/libX11
            x11-libs/libXext
        )
        fpx? ( media-libs/libfpx )
        heif? ( media-libs/libheif )
        imagemagick? (
            !media-gfx/ImageMagick [[
                description = [ GraphicsMagick is a fork of ImageMagick ]
                resolution = uninstall-blocked-after
            ]]
        )
        jpeg2000? ( media-libs/jasper )
        jpegxl? ( media-libs/libjxl:=[>=0.7rc] )
        lcms? ( media-libs/lcms2 )
        openmp? ( sys-libs/libgomp:= )
        providers:ijg-jpeg? ( media-libs/jpeg:=[>=6b] )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        tiff? ( media-libs/tiff:=[>=4.0.10][webp?][zstd?] )
        truetype? (
            fonts/corefonts
            fonts/urw-fonts
            media-libs/freetype:2
        )
        webp? ( media-libs/libwebp:= )
        wmf? ( media-libs/libwmf )
        zstd? ( app-arch/zstd[>=1.0.0] )
    suggestion:
        app-text/ghostscript [[ description = [ Support for reading PS, EPS and PDF formats ] ]]
        sci-apps/gnuplot [[ description = [ Support for reading GNUPlot files ] ]]
"

# TODO
# - configure.ac also has checks for: rst2html, txt2html p7zip, zip, but none of these are required
DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-compressed-files
    --enable-shared
    --disable-broken-coders
    # Disable OpenMP for algorithms which sometimes run slower
    --disable-openmp-slow
    --disable-static
    --with-bzlib
    --with-gs
    --with-gs-font-dir=/usr/share/fonts/X11/urw-fonts
    --with-jpeg
    --with-lzma
    --with-magick-plus-plus
    --with-modules
    --with-png
    --with-quantum-depth=16
    --with-threads
    --with-xml
    --with-zlib
    --without-gdi32
    --without-jbig
    --without-libzip
    --without-mtmalloc
    --without-perl
    --without-tcmalloc
    --without-trio
    --without-umem
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'imagemagick magick-compat'
    'openmp'
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    'X x'
    'fpx'
    'heif'
    'jpeg2000 jp2'
    'jpegxl jxl'
    'lcms lcms2'
    'tiff'
    'truetype ttf'
    'truetype windows-font-dir /usr/share/fonts/X11/corefonts'
    'webp'
    'wmf'
    'zstd'
)

src_install() {
    default

    if ! option doc; then
        edo rm -r "${IMAGE}"/usr/share/doc/${PNVR}/www
    fi
}

