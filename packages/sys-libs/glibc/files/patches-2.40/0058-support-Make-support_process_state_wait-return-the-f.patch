Upstream: yes, taken from release/2.40/master

From 846e64257e5fc9b5b723c2eec2b7155ab5944d1f Mon Sep 17 00:00:00 2001
From: Adhemerval Zanella <adhemerval.zanella@linaro.org>
Date: Fri, 13 Sep 2024 11:10:05 -0300
Subject: [PATCH 58/62] support: Make support_process_state_wait return the
 found state

So caller can check which state was found if multiple ones are
asked.

Checked on x86_64-linux-gnu.

Reviewed-by: Florian Weimer <fweimer@redhat.com>
(cherry picked from commit 38316352e0f742f3a2b5816a61a4b603cb5573f8)
(cherry picked from commit 2e38c5a090b3a54040b6e508d42e5a76e492c6e8)
---
 support/process_state.h             |  7 +++++--
 support/support_process_state.c     |  6 ++++--
 support/tst-support-process_state.c | 19 +++++++++++++++----
 3 files changed, 24 insertions(+), 8 deletions(-)

diff --git a/support/process_state.h b/support/process_state.h
index 1cf902e91b..9541d8c343 100644
--- a/support/process_state.h
+++ b/support/process_state.h
@@ -31,13 +31,16 @@ enum support_process_state
   support_process_state_dead         = 0x20,  /* X (dead).  */
   support_process_state_zombie       = 0x40,  /* Z (zombie).  */
   support_process_state_parked       = 0x80,  /* P (parked).  */
+  support_process_state_invalid      = 0x100  /* Invalid state.  */
 };
 
 /* Wait for process PID to reach state STATE.  It can be a combination of
    multiple possible states ('process_state_running | process_state_sleeping')
    where the function return when any of these state are observed.
    For an invalid state not represented by SUPPORT_PROCESS_STATE, it fallbacks
-   to a 2 second sleep.  */
-void support_process_state_wait (pid_t pid, enum support_process_state state);
+   to a 2 second sleep.
+   Return the found process state.  */
+enum support_process_state
+support_process_state_wait (pid_t pid, enum support_process_state state);
 
 #endif
diff --git a/support/support_process_state.c b/support/support_process_state.c
index 062335234f..ae8e0a531c 100644
--- a/support/support_process_state.c
+++ b/support/support_process_state.c
@@ -27,7 +27,7 @@
 #include <support/xstdio.h>
 #include <support/check.h>
 
-void
+enum support_process_state
 support_process_state_wait (pid_t pid, enum support_process_state state)
 {
 #ifdef __linux__
@@ -75,7 +75,7 @@ support_process_state_wait (pid_t pid, enum support_process_state state)
 	  {
 	    free (line);
 	    xfclose (fstatus);
-	    return;
+	    return process_states[i].s;
 	  }
 
       rewind (fstatus);
@@ -90,4 +90,6 @@ support_process_state_wait (pid_t pid, enum support_process_state state)
   /* Fallback to nanosleep if an invalid state is found.  */
 #endif
   nanosleep (&(struct timespec) { 1, 0 }, NULL);
+
+  return support_process_state_invalid;
 }
diff --git a/support/tst-support-process_state.c b/support/tst-support-process_state.c
index d73269320f..4a88eae3a7 100644
--- a/support/tst-support-process_state.c
+++ b/support/tst-support-process_state.c
@@ -68,28 +68,39 @@ do_test (void)
   if (test_verbose)
     printf ("info: waiting pid %d, state_stopped/state_tracing_stop\n",
 	    (int) pid);
-  support_process_state_wait (pid, stop_state);
+  {
+    enum support_process_state state =
+      support_process_state_wait (pid, stop_state);
+    TEST_VERIFY (state == support_process_state_stopped
+		 || state == support_process_state_tracing_stop);
+  }
 
   if (kill (pid, SIGCONT) != 0)
     FAIL_RET ("kill (%d, SIGCONT): %m\n", pid);
 
   if (test_verbose)
     printf ("info: waiting pid %d, state_sleeping\n", (int) pid);
-  support_process_state_wait (pid, support_process_state_sleeping);
+  TEST_COMPARE (support_process_state_wait (pid,
+					    support_process_state_sleeping),
+		support_process_state_sleeping);
 
   if (kill (pid, SIGUSR1) != 0)
     FAIL_RET ("kill (%d, SIGUSR1): %m\n", pid);
 
   if (test_verbose)
     printf ("info: waiting pid %d, state_running\n", (int) pid);
-  support_process_state_wait (pid, support_process_state_running);
+  TEST_COMPARE (support_process_state_wait (pid,
+					    support_process_state_running),
+		support_process_state_running);
 
   if (kill (pid, SIGKILL) != 0)
     FAIL_RET ("kill (%d, SIGKILL): %m\n", pid);
 
   if (test_verbose)
     printf ("info: waiting pid %d, state_zombie\n", (int) pid);
-  support_process_state_wait (pid, support_process_state_zombie);
+  TEST_COMPARE (support_process_state_wait (pid,
+					    support_process_state_zombie),
+		support_process_state_zombie);;
 
   siginfo_t info;
   int r = waitid (P_PID, pid, &info, WEXITED);
-- 
2.47.1

