# Copyright 2023 Morgane “Sardem FF7” Glidic <sardemff7@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require alternatives

SUMMARY="The best way to manage a Ruby application's gems"

LICENCES="|| ( Ruby-1.9 BSD-2 )"
SLOT="builtin"
PLATFORMS="~amd64 ~armv7 ~armv8"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-lang/ruby:*
    run:
        !dev-ruby/bundler:0 [[
            description = [ Alternatives conflict ]
            resolution = uninstall-blocked-before
        ]]
"

WORK=${WORKBASE}

src_install() {
    local bin bundler_alternatives=()
    for bin in bundle{,r}; do
        bundler_alternatives+=( /usr/$(exhost --target)/bin/${bin} ${bin}-builtin )
    done
    alternatives_for bundler ruby-builtin 1000 "${bundler_alternatives[@]}"
}
