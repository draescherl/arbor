# Copyright 2016 Clever Cloud
# Distributed under the terms of the GNU General Public License v2

require gem

myexparam blacklist=none

SUMMARY="The best way to manage a Ruby application's gems"

LICENCES="MIT"
SLOT="$(ever major)"
MYOPTIONS=""

DEPENDENCIES="
    run:
        !dev-ruby/bundler:0 [[
            description = [ Alternatives conflict ]
            resolution = uninstall-blocked-before
        ]]
"

install_one_multibuild() {
    gem_install_one_multibuild

    local abi=$(ruby_get_abi)
    local alternatives=()
    local slot_alternatives=()
    local bindir=usr/$(exhost --target)/bin
    local gembindir=usr/$(exhost --target)/lib/ruby/vendor_ruby/gems/${abi}/bin

    edo cd "${IMAGE}"
    local bin
    for bin in bundle{,r}; do
        edo rm ${bindir}/${bin}${abi}
        edo mv ${gembindir}/${bin}{,-${SLOT}}
        alternatives+=( /${bindir}/${bin}-${SLOT} /${gembindir}/${bin}-${SLOT} )
        slot_alternatives+=( /${bindir}/${bin} /${bindir}/${bin}-${SLOT} )
    done
    edo rm -rf ${bindir}

    alternatives_for ${PN}-${SLOT} ${abi} ${abi} "${alternatives[@]}"
    alternatives_for ${PN} ${SLOT} ${SLOT} "${slot_alternatives[@]}"
}
